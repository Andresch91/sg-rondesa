﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DataBase;

namespace GenerateDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var Proveedor01 = new Proveedor()
            {
                Representante = "Ingeniero",
                Correo = "Area@hotmail.com",
                RazonSocial = "Coop.Rondesa",
                Telefono="356673"
            };

            var _context = new RondesaContex();
            Console.WriteLine("Creando Base De Datos");
            _context.Proveedores.Add(Proveedor01);            
            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}
