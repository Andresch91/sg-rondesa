﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Producto
    {
        public Int32 IdProducto { get; set; }
        public string Nombre { get; set; }
        public string Modelo { get; set; }
        public string Serie { get; set; }
        public string Marca { get; set; }
        public decimal Stock { get; set; }
        public string Color { get; set; }
        public DateTime FechaIngreso { get; set; }
        public decimal Cantidad { get; set; }
        public string UnidadMedida { get; set; }

        public Int32 IdProveedorFk { get; set; }
        public Proveedor Proveedor { get; set; }

        public Int32 IdTipoProductoFk { get; set; }
        public TipoProducto TipoProducto { get; set; }

    }
}
