﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Agencia
    {
        public Int32 IdAgencia { get; set; }
        public string Nombre { get; set; }
        public Int32 CodigoAgencia { get; set; }

        public Int32 IdAreaFk { get; set; }
        public Area Area { get; set; }
    }
}
