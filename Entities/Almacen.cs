﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Almacen
    {
        public Int32 IdAlmacen { get; set; }
        public Int32 IdProductoFk { get; set; }
        public Producto Producto { get; set; }

        public Int32 IdMovimientoFk { get; set; }
        public Movimientos Movimientos { get; set; }
    }
}
