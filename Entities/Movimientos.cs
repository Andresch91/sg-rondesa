﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Movimientos
    {
        public Int32 IdMovimientos { get; set; }
        public DateTime Fecha { get; set; }
        public string Tipo { get; set; }
        public int NroMovimiento { get; set; }

    }
}
