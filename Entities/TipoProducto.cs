﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class TipoProducto
    {
        public Int32 IdTipoProducto { get; set; }
        public string Descripcion { get; set; }

    }
}
