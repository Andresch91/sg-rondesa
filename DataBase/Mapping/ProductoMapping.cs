﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class ProductoMapping: EntityTypeConfiguration<Producto>
    {
       public ProductoMapping()
       {
           this.HasKey(p => p.IdProducto);
           this.Property(p => p.IdProducto).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Nombre).IsRequired().HasMaxLength(100);
           this.Property(p => p.Modelo).IsRequired().HasMaxLength(50);
           this.Property(p => p.Serie).IsRequired();
           this.Property(p => p.Marca).IsRequired().HasMaxLength(50);
           this.Property(p => p.Stock).IsRequired();
           this.Property(p => p.Color).IsRequired().HasMaxLength(50);
           this.Property(p => p.FechaIngreso).IsRequired();
           this.Property(p => p.Cantidad).IsRequired();
           this.Property(p => p.UnidadMedida).IsRequired();

           this.HasRequired(p => p.Proveedor).WithMany().HasForeignKey(p => p.IdProveedorFk).WillCascadeOnDelete(true);
           this.HasRequired(p => p.TipoProducto).WithMany().HasForeignKey(p => p.IdTipoProductoFk).WillCascadeOnDelete(true);

           this.ToTable("Producto");
       }
    }
}
