﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class DepreciacionMap:EntityTypeConfiguration<Depreciacion>
    {
       public DepreciacionMap()
       {
           this.HasKey(p => p.IdDepreciacion);
           this.Property(p => p.IdDepreciacion).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.ToTable("Depreciacion");
       }
    }
}
