﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class BienesMap:EntityTypeConfiguration<Bienes>
    {
       public BienesMap()
       {
           this.HasKey(p => p.Idbienes);
           this.Property(p => p.Idbienes).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.ToTable("Bienes");
       }
    }
}
