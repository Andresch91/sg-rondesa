﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class TipoProductoMap:EntityTypeConfiguration<TipoProducto>
    {
       public TipoProductoMap()
       {
           this.HasKey(p => p.IdTipoProducto);
           this.Property(p => p.IdTipoProducto).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.ToTable("Tipoproducto");
       }
    }
}
