﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class MovimientosMap:EntityTypeConfiguration<Movimientos>
    {
       public MovimientosMap()
       {
           this.HasKey(p => p.IdMovimientos);
           this.Property(p => p.IdMovimientos).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


           this.ToTable("Movimientos");
       }
    }
}
