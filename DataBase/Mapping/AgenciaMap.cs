﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class AgenciaMap:EntityTypeConfiguration<Agencia>
    {
       public AgenciaMap()
       {
           this.HasKey(p => p.IdAgencia);
           this.Property(p => p.IdAgencia).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);         

           this.ToTable("Agencia");
       }
    }
}
